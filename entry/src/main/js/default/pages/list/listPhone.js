import router from '@system.router';
export default {
    data: {
        phoneList: [
            {id: 1, title: "测试标题1", img: "common/images/1.jpg"},
            {id: 2, title: "测试标题2", img: "common/images/2.jpg"},
            {id: 3, title: "测试标题3", img: "common/images/3.jpg"},
            {id: 4, title: "测试标题4", img: "common/images/4.jpg"},
            {id: 5, title: "测试标题5", img: "common/images/5.jpg"},
            {id: 6, title: "测试标题6", img: "common/images/6.jpg"},
            {id: 7, title: "测试标题7", img: "common/images/7.jpg"},
            {id: 8, title: "测试标题8", img: "common/images/8.jpg"},
            {id: 9, title: "测试标题9", img: "common/images/9.jpg"},
            {id: 10, title: "测试标题10", img: "common/images/10.jpg"}
        ]
    },

    goDetail(nId) {
        router.push({
            uri: 'pages/detailPhone/detailPhone',
            params: {
                id: nId,
                title: this.phoneList[nId-1].title
            }
        })
    }
}